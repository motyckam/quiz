//Tvorba objektu obsahující otázky a odpovědi
let vsechnyKategorie = {
    kategorie1: [
        {
            otazka: "Která automobilka vyrábí model 'Civic'?",
            odpovedi: [
                { text: "Toyota", spravnyVysledek: false },
                { text: "Honda", spravnyVysledek: true },
                { text: "Nissan", spravnyVysledek: false },
                { text: "Mazda", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Který z těchto modelů je sportovní vůz značky Porsche?",
            odpovedi: [
                { text: "911", spravnyVysledek: true },
                { text: "320i", spravnyVysledek: false },
                { text: "CR-V", spravnyVysledek: false },
                { text: "S60", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Jaký typ paliva používá většina moderních dieselových vozidel?",
            odpovedi: [
                { text: "Benzín", spravnyVysledek: false },
                { text: "Elektřina", spravnyVysledek: false },
                { text: "Nafta", spravnyVysledek: true },
                { text: "Vodík", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Který z těchto výrobců pochází z Itálie?",
            odpovedi: [
                { text: "BMW", spravnyVysledek: false },
                { text: "Ford", spravnyVysledek: false },
                { text: "Ferrari", spravnyVysledek: true },
                { text: "Volvo", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Který z následujících automobilů je známý svým 'gullwing' designem dveří?",
            odpovedi: [
                { text: "Chevrolet Corvette", spravnyVysledek: false },
                { text: "Mercedes-Benz 300SL", spravnyVysledek: true },
                { text: "Ford Mustang", spravnyVysledek: false },
                { text: "Audi A4", spravnyVysledek: false }
            ]
        }
    ],
    kategorie2: [
        {
            otazka: "Kdy začala druhá světová válka?",
            odpovedi: [
                { text: "1914", spravnyVysledek: false },
                { text: "1939", spravnyVysledek: true },
                { text: "1945", spravnyVysledek: false },
                { text: "1950", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Kdo byl prvním prezidentem Spojených států?",
            odpovedi: [
                { text: "Thomas Jefferson", spravnyVysledek: false },
                { text: "John Adams", spravnyVysledek: false },
                { text: "Abraham Lincoln", spravnyVysledek: false },
                { text: "George Washington", spravnyVysledek: true }
            ]
        },
        {
            otazka: "Který slavný francouzský vůdce byl poražen u Waterloo?",
            odpovedi: [
                { text: "Louis XIV", spravnyVysledek: false },
                { text: "Napoleon Bonaparte", spravnyVysledek: true },
                { text: "Charlemagne", spravnyVysledek: false },
                { text: "Charles de Gaulle", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Ve kterém roce se rozpadl Sovětský svaz?",
            odpovedi: [
                { text: "1989", spravnyVysledek: false },
                { text: "1990", spravnyVysledek: false },
                { text: "1991", spravnyVysledek: true },
                { text: "1992", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Která válka byla známá jako 'Velká válka'?",
            odpovedi: [
                { text: "Americká občanská válka", spravnyVysledek: false },
                { text: "První světová válka", spravnyVysledek: true },
                { text: "Druhá světová válka", spravnyVysledek: false },
                { text: "Korejská válka", spravnyVysledek: false }
            ]
        }
    ],
    kategorie3: [
        {
            otazka: "Jaký je největší ostrov na světě?",
            odpovedi: [
                { text: "Grónsko", spravnyVysledek: true },
                { text: "Nová Guinea", spravnyVysledek: false },
                { text: "Borneo", spravnyVysledek: false },
                { text: "Madagaskar", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Která řeka je nejdelší na světě?",
            odpovedi: [
                { text: "Amazonka", spravnyVysledek: true },
                { text: "Mississippi", spravnyVysledek: false },
                { text: "Nil", spravnyVysledek: false },
                { text: "Jang-c’-ťiang", spravnyVysledek: false }
            ]
        },
        {
            otazka: "Které město je hlavním městem Austrálie?",
            odpovedi: [
                { text: "Sydney", spravnyVysledek: false },
                { text: "Melbourne", spravnyVysledek: false },
                { text: "Brisbane", spravnyVysledek: false },
                { text: "Canberra", spravnyVysledek: true }
            ]
        },
        {
            otazka: "Jak se jmenuje nejvyšší hora světa?",
            odpovedi: [
                { text: "K2", spravnyVysledek: false },
                { text: "Mount Everest", spravnyVysledek: true },
                { text: "Kilimandžáro", spravnyVysledek: false },
                { text: "Mont Blanc", spravnyVysledek: false }
            ]
        },
        {
            otazka: "V které zemi se nachází Velká čínská zeď?",
            odpovedi: [
                { text: "Japonsko", spravnyVysledek: false },
                { text: "Čína", spravnyVysledek: true },
                { text: "Jižní Korea", spravnyVysledek: false },
                { text: "Mongolsko", spravnyVysledek: false }
            ]
        }
    ]
};

let aktualniKategorie;          //aktuálně vybraná kategorie
let aktualniKategorieKlic;      //klíč aktuálně vybrané kategorie
let aktualniOtazkaIndex = 0;    //index aktuální otázky v kategorii
let spravneOdpovedi = 0;        //ukládání počtu správných odpovědí
let quizStartCasu;              //čas začátku kvízu
let zbyvajiciCas;               //zbývající čas kvízu
let casovac;                    //časovač


/**
 * výběr obtížnosti
 * @param {*} obtiznost 
 */
function vyberObtiznost(obtiznost) {
    localStorage.setItem('quizObtiznost', obtiznost);                                   //uloží vybranou obtížnost do localStorage
    document.getElementById('obtiznost-container').classList.add('hide');               //skrytí výběru obtížnosti
    document.getElementById('kategorie-container').classList.remove('hide');            //zobrazí kontejner pro výběr kategorie
}


/**
 * nastavení časového limitu podle obtížnosti
 * @param {*} obtiznost 
 */
function nastavCasovyLimit(obtiznost) {
    if (obtiznost === 'zakladni') {
        zbyvajiciCas = 120; 
    } else if (obtiznost === 'pokrocily') {
        zbyvajiciCas = 60;
    }
    aktualizujZobrazenyCas();
}


/**
 * start kvízu podle vybrané kategorie a obtížnosti
 * @param {*} kategorie 
 * @param {*} obtiznost 
 */
function startQuiz(kategorie, obtiznost) {
    nastavCasovyLimit(obtiznost);                           //set času podle obtížnosti
    aktualniKategorieKlic = kategorie;                      //aktuální klíč kategorie
    aktualniKategorie = vsechnyKategorie[kategorie];        //aktuální kategorie otázek
    aktualniOtazkaIndex = 0;                                //vynuluje aktuální otázku
    spravneOdpovedi = 0;                                    //vynuluje počet správných odpovědí

    //odkazuje na HTML
    let quizContainer = document.getElementById('quiz-container');
    let tlacitkoDalsi = document.getElementById('dalsi-btn');
    let tlacitkoUkoncit = document.getElementById('ukoncit-btn');

    quizContainer.classList.remove('hide');                 //zobrazí kvíz          
    tlacitkoDalsi.classList.add('hide');                    //skryje tlačítko další
    tlacitkoUkoncit.classList.add('hide');                  //skryje tlačítko ukončit

    zapniCasovac();                                         //zapne čas
    ukazOtazku();                                           //ukáže otázku
}


/**
 * spuštění časovače
 */
function zapniCasovac() {
    quizStartCasu = Date.now();                             //uloží čas
    casovac = setInterval(aktualizujCasovac, 1000);         //odečítá každou vteřinu
}

//
/**
 * aktualizuje zobrazený čas
 */
function aktualizujZobrazenyCas() {
    let minuty = Math.floor(zbyvajiciCas / 60);                                 //počet minut
    let sekundy = zbyvajiciCas % 60;                                            //zbývající vteřiny
    let formatovanyCas = `${minuty}:${sekundy < 10 ? '0' : ''}${sekundy}`;      //--:-- formát

    let ziskaniCasu = document.getElementById('cas');                           //odkaz na HTML
    ziskaniCasu.textContent = formatovanyCas;                       
}


/**
 * aktualizuje časovač
 */
function aktualizujCasovac() {
    zbyvajiciCas--;                                         //odečte vteřinu

    aktualizujZobrazenyCas();                              

    if (zbyvajiciCas <= 0) {                                //cyklus, který ukončí kvíz v případě vypršení času 
        clearInterval(casovac);
        ukonciQuiz(true);
    }
}

/**
 * Zobrazování otázek
 */
function ukazOtazku() {
    resetuj();                                                                      //reset pro zobrazení nové otázky
    let otazka = aktualniKategorie[aktualniOtazkaIndex];                            //načtení otázky

    let textOtazky = otazka.otazka;                                                 //načtení textu
    document.getElementById('otazka').textContent = textOtazky;                     //odkaz na HTML

    //cyklus pro odpovědi k otázce
    for (let i = 0; i < otazka.odpovedi.length; i++) {
        let odpoved = otazka.odpovedi[i];                                           //načte odpověď
        let tlacitko = document.createElement('button');                            //vytvoří tlačítko
        tlacitko.innerText = odpoved.text;                                          //načte text do tlačítka
        tlacitko.classList.add('btn');                                              //css tlačítka
        if (odpoved.spravnyVysledek) {                                              //zaznamená, zda byla odpověď správná nebo ne
            tlacitko.dataset.spravnyVysledek = odpoved.spravnyVysledek;
        }
        tlacitko.addEventListener('click', vyberOdpoved);                           //po kliknutí spouští vyberOdpoved
        let odpovediContainer = document.getElementById('odpoved-buttons');         //odkaz na HTML, přidá tlačítko
        odpovediContainer.appendChild(tlacitko);
    }
}

/**
 * Resetování
 */
function resetuj() {
    let tlacitkoDalsi = document.getElementById('dalsi-btn');                       //odkaz na HTML
    tlacitkoDalsi.classList.add('hide');                                            //skryje tlačítko další

    let tlacitkoUkoncit = document.getElementById('ukoncit-btn');                   //odkaz na HTML
    tlacitkoUkoncit.classList.add('hide');                                          //skryje tlačítko ukončit

    let tlacitkoOdpoved = document.getElementById('odpoved-buttons');               //odkaz na HTML
    tlacitkoOdpoved.innerHTML = '';                                                 //vymaže prostor pro odpovědi
}


/**
 * zpracování výběru odpovědi
 * @param {*} e 
 */
function vyberOdpoved(e) {
    let zvoleneTlacitko = e.target;                                                 //načte nakliknuté tlačítko
    let spravnyVysledek = zvoleneTlacitko.dataset.spravnyVysledek === 'true';       //ověří správnost odpovědi

    if (spravnyVysledek) {                                                          //v případě správné odpovědi přičte počet správných odpovědí
        spravneOdpovedi++;
    }

    let tlacitka = document.querySelectorAll('#odpoved-buttons > button');          //načte tlačítka odpovědí

    for (let i = 0; i < tlacitka.length; i++) {                                     //cyklus pro všechny tlačítka
        let tlacitko = tlacitka[i];                                                 //načte aktuální tlačítko
        let jeSpravnyVysledek = tlacitko.dataset.spravnyVysledek === 'true';        //ověří správnost
        setStatusClass(tlacitko, jeSpravnyVysledek);                                //podle správnosti nastaví třídu
        tlacitko.disabled = true;                                                   //deaktivuje tlačítko
    }

    if (aktualniKategorie.length > aktualniOtazkaIndex + 1) {                       //pro další otázku zobrazí tlačítko další, pokud další není zobrazí tlačítko ukončit
        document.getElementById('dalsi-btn').classList.remove('hide');
    } else {
        document.getElementById('ukoncit-btn').classList.remove('hide');
    }
}


/**
 * nastaví třídu podle správnosti odpovědi
 * @param {*} element 
 * @param {*} spravnyVysledek 
 */
function setStatusClass(element, spravnyVysledek) {
    clearStatusClass(element);                          //vymaže třídu
    if (spravnyVysledek) {                              //přidá třídu správně nebo špatně
        element.classList.add('spravne');
    } else {
        element.classList.add('spatne');
    }
}


/**
 * maže třídy
 * @param {*} element 
 */
function clearStatusClass(element) {
    element.classList.remove('spravne');
    element.classList.remove('spatne');
}


/**
 * zobrazuje další otázky
 */
function ukazDalsiOtazku() {
    aktualniOtazkaIndex++;
    ukazOtazku();
}


/**
 * ukončuje kvíz
 * @param {*} vyprselCas 
 * @returns 
 */
function ukonciQuiz(vyprselCas) {
    clearInterval(casovac);                                                             //zastaví časovač

    if (vyprselCas) {                                                                   //v případě vypršení času skryje kvíz a oznámí vypršení času
        document.getElementById('quiz-container').classList.add('hide');
        document.getElementById('vysledky-seznam').innerHTML = '<h2>Čas vypršel.</h2>';
        return;
    }

    let uplynulyCas = 120 - zbyvajiciCas;                                               //vypočte jak dlouho kvíz trval
    let vysledky = {                                                                    //nastaví data z vyplněného kvízu
        kategorie: aktualniKategorieKlic,                                               
        spravneOdpovedi: spravneOdpovedi,
        otazkyCelkem: aktualniKategorie.length,
        uplynulyCas: uplynulyCas
    };

    ukazAVyhledejVysledky(vysledky);                                                    //uloží výsledky
}


/**
 * zobrazí a vyhledá
 * @param {*} vysledky 
 */
function ukazAVyhledejVysledky(vysledky) {
    let jmeno = prompt("Zadejte Vaše jméno");                                           //vyzve k zadání jména

    let ulozeneVysledky = JSON.parse(localStorage.getItem('quizVysledky')) || [];       //získá výsledky z localStorage
    ulozeneVysledky.push({ jmeno, ...vysledky });                                       //přidá výsledky
    localStorage.setItem('quizVysledky', JSON.stringify(ulozeneVysledky));              //opět uloží do localStorage

    ukazVysledky(vysledky, jmeno);                                                      //zobrazí výsledky
}


/**
 * zobrazuje výsledky na stránce
 * @param {*} vysledky 
 * @param {*} jmeno 
 */
function ukazVysledky(vysledky, jmeno) {
    let quizContainer = document.getElementById('quiz-container');                      //odkaz na HTML
    quizContainer.classList.add('hide');                                                //skryje kontejner kvízu
    
    let vysledkyContainer = document.getElementById('vysledky-container');              //odkaz na HTML
    vysledkyContainer.classList.remove('hide');                                         //ukáže výsledky
}


/**
 * získání názvu kategorie
 * @param {*} klicKategorie 
 * @returns 
 */
function ziskejJmenoKategorie(klicKategorie) {
    switch (klicKategorie) {
        case 'kategorie1':
            return 'Kategorie 1';
        case 'kategorie2':
            return 'Kategorie 2';
        case 'kategorie3':
            return 'Kategorie 3';
        default:
            return 'Neznama Kategorie';
    }
}


/**
 * formát času --:--
 * @param {*} sekundy 
 * @returns 
 */
function formatCasu(sekundy) {
    let minuty = Math.floor(sekundy / 60);                                              //vypočte počet minut
    let zbyvajiciSekundy = sekundy % 60;                                                //zbývající sekundy
    return `${minuty}:${zbyvajiciSekundy < 10 ? '0' : ''}${zbyvajiciSekundy}`;          //vrací ve formátu --:--
}


/**
 * aktualizuje výsledky na stránce
 */
function aktualizujVysledky() {
    let ulozeneVysledky = JSON.parse(localStorage.getItem('quizVysledky')) || [];           //načte výsledky z localstorage
    let vysledkyTabulka = document.querySelector('#vysledky-tabulka tbody');                //načte tabulku s výsledky
    vysledkyTabulka.innerHTML = '';                                                         //vymaže tabulku výsledků

    for (let i = 0; i < ulozeneVysledky.length; i++) {                                      //tvorba tabulky
        let row = document.createElement('tr');                                             //nový řádek
    
        let jmenoPolicko = document.createElement('td');                                    //nové políčko
        jmenoPolicko.textContent = ulozeneVysledky[i].jmeno;                                //nastavení textu
        row.appendChild(jmenoPolicko);                                                      //přidání políčka
    
        let kategoriePolicko = document.createElement('td');                                //nové políčko
        kategoriePolicko.textContent = ziskejJmenoKategorie(ulozeneVysledky[i].kategorie);  //nastavení textu
        row.appendChild(kategoriePolicko);                                                  //přidání políčka
    
        let spravneOdpovediPolicko = document.createElement('td');                          //nové políčko
        spravneOdpovediPolicko.textContent = ulozeneVysledky[i].spravneOdpovedi;            //nastavení textu
        row.appendChild(spravneOdpovediPolicko);                                            //přidání políčka
    
        let otazkyCelkemPolicko = document.createElement('td');                             //nové políčko
        otazkyCelkemPolicko.textContent = ulozeneVysledky[i].otazkyCelkem;                  //nastavení textu
        row.appendChild(otazkyCelkemPolicko);                                               //přidání políčka
    
        let uplynulyCasPolicko = document.createElement('td');                              //nové políčko
        uplynulyCasPolicko.textContent = formatCasu(ulozeneVysledky[i].uplynulyCas);        //nastavení textu
        row.appendChild(uplynulyCasPolicko);                                                //přidání políčka
    
        vysledkyTabulka.appendChild(row);                                                   //přidá řádek
    }
}

window.onload = function() {                        //aktualizuje výsledky při načtení stránky
    aktualizujVysledky();
};